// Макет: https://www.figma.com/file/Do0TLndoEjGwuF9Ri7UHol/The-Ham-Step-Project?node-id=0%3A1&mode=dev */

// Завдання
// Зверстати макет. Підключити динамічні елементи (див. нижче)

// Технічні вимоги до верстки
// + Потрібно зробити верстку тільки під широкоформатні монітори з шириною екрану 1200 пікселів або більше 
// (тобто ширина контенту фіксована в пікселях). Максимальна ширина контейнера з контентом – 1160 пікселів (крім другого блоку з квадратами).
// + Фонове зображення в шапці повинне займати всю доступну ширину екрана і не рухатися під час скролу вниз (ефект паралаксу).
// + Адаптивна верстка під різні роздільні здатності екрана НЕ потрібна.
// + Картки в секції Breaking news мають бути клікабельними посиланнями.
// + Секція Gallery of best images не є обов'язковою для виконання.
// Верстка має бути виконана без використання CSS бібліотек типу Bootstrap або Materialize.
// 
// Динамічні елементи на сторінці:
// + Вкладки у секції Our services повинні перемикатися при натисканні мишею. Текст та картинки для інших вкладок додати будь-які.
// + Кнопка Load more у секції Our amazing work імітує завантаження з сервера нових картинок. 
// + При її натисканні в секції знизу мають з'явитись ще 12 картинок (зображення можна взяти будь-які). Після цього кнопка зникає.
// + Кнопки на вкладці Our amazing work є "фільтрами продукції". 
// Попередньо кожній із картинок потрібно присвоїти одну з чотирьох категорій, на ваш розсуд 
// (на макеті це Graphic design, Web design, Landing pages, Wordpress).При натисканні на кнопку категорії необхідно показати лише ті картинки, 
// які належать до цієї категорії. All показує картинки з усіх категорій. Категорії можна перейменувати, картинки для категорій взяти будь-які.
// + Карусель на вкладці What people say about theHam має бути робочою, по кліку як на іконку фотографії внизу, так і на стрілки вправо-вліво. 
// У каруселі має змінюватися як картинка, і текст. Карусель обов'язково має бути з анімацією.
// Для підключення динамічних елементів можна використовувати будь-які бібліотеки – як jQuery/його плагіни, так і чистий Javascript код.
// 
// Необов'язкові завдання підвищеної складності:
// + Кнопку Load more у секції Our amazing work можна натиснути двічі, кожне натискання додає 12 картинок знизу. 
// Тобто максимум у цій секції може бути 36 картинок. Після другого натискання кнопка зникає.
// + Зверстати також секцію Gallery of best images, розташувати картинки всередині блоку за допомогою плагіну Masonry.
// - Кнопка Load more у секції Gallery of best images також має бути робочою та додавати порцію нових картинок на сторінку.
// - При натисканні на кожну з кнопок Load more імітувати завантаження картинок із сервера. 
// Показувати замість кнопки або над нею дві секунди CSS анімацію завантаження 
// (можна написати самому або взяти будь-який приклад з інтернету, наприклад тут або тут), і лише після цього додавати картинки на сторінку.
// Розмістити проект в інтернеті за допомогою Github pages або Gitlab pages (Не забудьте потім додати посилання на резюме).

// 1. Секція Our Servises
// додаю клас з відповідними індексами в другий список з текстами та приховую видимість для всіх li:
let ServiseItems = document.querySelectorAll(".servise-item");
console.log(ServiseItems);

for (let index = 0; index < ServiseItems.length; index++) {
    ServiseItems[index].setAttribute("data-servise-item-id", index);
    // ServiseItems[index].hidden = true;  
    // ServiseItems[0].hidden = false;
    ServiseItems[index].style.display = "none";
    ServiseItems[0].style.display = "flex";
}

// додаю клас з відповідними індексами в перший список з меню,
// а також в циклі додаю кожній li властивість з функцією зміни класа active:
let ulServiseMenu = document.querySelector(".servise-menu");
console.log(ulServiseMenu);
console.log(ulServiseMenu.children);

let ulServiseMenuObject = Array.from(ulServiseMenu.children);
console.log(ulServiseMenuObject);

for (let index = 0; index < ulServiseMenuObject.length; index++) {
    ulServiseMenuObject[index].setAttribute("data-servise-menu-id", index);
    ulServiseMenuObject[index].addEventListener('click', changeClassActive);
}

document.addEventListener("DOMContentLoaded", () => {
    const firstServiseMenuItem = document.querySelector(".servise-menu-item");
    firstServiseMenuItem.style.backgroundColor = "var(--color1)";
    firstServiseMenuItem.style.color = "var(--color2)";

    const firstTriangle = document.querySelector(".triangle");
    firstTriangle.style.borderTop = "27px solid var(--color1)";
})

// створюю функцію, яка прибирає всім і додає по кліку одному клас active в першому списку з меню,    
// а також по кліку фарбує в зелений фон і в білий шрифт відповідному елемент меню, 
// та змінює стиль трикутника, щоб він з'явився
function changeClassActive(event) {

    let eventTargetLiMenu = event.target.closest("li"); // шукаємо найближчу li по кліку
    let eventTargetParagrafMenu = eventTargetLiMenu.querySelector(".servise-menu-item"); // шукаємо в найближчому li по кліку p
    console.log(eventTargetParagrafMenu);

    let eventTargetTriangle = eventTargetLiMenu.querySelector(".triangle"); // шукаємо в найближчому li по кліку div з трикутником
    console.log(eventTargetTriangle);

    ulServiseMenuObject.forEach(item => {
        item.classList.remove("active");

        let paragraf = item.querySelector("p.servise-menu-item");
        paragraf.style.backgroundColor = "";
        paragraf.style.color = "";

        let triangle = item.querySelector(".triangle");
        triangle.style.borderTop = "";
    });

    eventTargetLiMenu.classList.add("active");
    console.log(eventTargetLiMenu);

    eventTargetParagrafMenu.style.backgroundColor = "var(--color1)";
    eventTargetParagrafMenu.style.color = "var(--color2)";
    eventTargetTriangle.style.borderTop = "27px solid var(--color1)";


    let generalTargetIndex = eventTargetLiMenu.dataset.serviseMenuId;
    console.log(generalTargetIndex); // ми отримали індекс li з першого списку, на яку клікнули

    console.log(ServiseItems[generalTargetIndex]);
    ServiseItems.forEach(item => item.style.display = "none");
    ServiseItems[generalTargetIndex].style.display = "flex";
}




// 2. Секція Our Amazing Work
// Дістаємо необхідні кнопку, колекцію картинок, картинки, навігаційний список (фільтр), пункти фільтру-навігації
let buttonWork = document.querySelector(".button-work");  // кнопка
console.log(buttonWork);

let workImages = document.querySelector(".work-images");  // контейнер з картинками
console.log(workImages);

let workImagesLi = document.querySelectorAll(".work-images li");  // картинки
console.log(workImagesLi);

let workFilter = document.querySelector(".work-filter");  // навігаційний список (фільтр)
console.log(workFilter);

let workFilterLi = document.querySelectorAll(".work-filter li");  // пункти фільтру-навігації
console.log(workFilterLi);

let workSpinner = document.querySelector(".work-spinner");

let workImagesLiFirst = Array.from(document.querySelectorAll(".first"));  // картинки першої порції, до підгрузки інших картинок
console.log(workImagesLiFirst);

workImagesLi.forEach ((liItem) => {
    liItem.insertAdjacentHTML("beforeend", `<div class="work-images-hover">
    <a class="work-images-hover-svg" href="#"></a>
    <p class="green-text work-images-hover-creative">creative design</p>
    <p class="work-images-hover-web">Web Design</p>
    </div>`)
})

buttonWork.addEventListener("click", () => {

    workSpinner.style.display = "flex";
    setTimeout(() => workSpinner.style.display = "none", 2000);

    setTimeout(() => {
        workImagesLi.forEach((image) => {
            // image.hidden = false;
            if (image.classList.contains("second")) {
                image.hidden = false;
                // image.classList.add("first");
                image.classList.remove("second");
                image.classList.add("first");  // додаємо клас first картинкам другої second порції
                workImagesLiFirst.push(image);  // додаємо картинки другої порції до масиву картинок
                console.log(workImagesLiFirst);
                // buttonWork.style.display = "none";
            } else if (image.classList.contains("third")) {
                image.hidden = true;
                image.classList.remove("third");
                image.classList.add("second");
                buttonWork.style.display = "flex";
            } else if (!image.classList.contains("third")) {
                buttonWork.style.display = "none";
            }
        });
    }, 2000);

    // buttonWork.style.display = "none";  // кнопка зникає
});

workFilter.addEventListener("click", (event) => {

    console.log(event.target);   // елемент фільтру, на який клікнули
    console.log(event.target.innerText);   // текстове значення елементу фільтру, на який клікнули

    workFilterLi.forEach((filterLi) => {
        filterLi.style.border = "1px solid var(--color15)";
        filterLi.style.color = "var(--color4)";
        filterLi.style.backgroundColor = "transparent";
        filterLi.classList.remove('active');
    })

    event.target.style.border = "1px solid var(--color1)";
    event.target.style.color = "var(--color2)";
    event.target.classList.add('active');
    event.target.style.backgroundColor = "var(--color1)";

    // Зміна фону і кольору шрифту до стилю натиснутого фільтра через 300 мілісекунд
    setTimeout(() => {
        event.target.style.color = "var(--color1)";
        event.target.style.backgroundColor = "transparent";
    }, 100);

    // переводимо текстове значення елементу, на який клікнули в нижній регистр та видаляємо всі пробіли зсередини
    let filterClass = event.target.innerText.toLowerCase().replace(/\s/g, '');
    console.log(filterClass);

    workImagesLiFirst.forEach((image) => {
        if (image.classList.contains(filterClass) || filterClass === 'all') {
            image.hidden = false;
        } else {
            image.hidden = true;
        }
    });

})

workFilter.addEventListener("mouseover", (event) => {
    if (event.target.tagName === 'LI') {
        event.target.style.border = "1px solid var(--color1)";
        event.target.style.color = "var(--color1)";
    }
});

workFilter.addEventListener("mouseout", (event) => {
    if (event.target.tagName === 'LI' && !event.target.classList.contains('active')) {
        event.target.style.border = "1px solid var(--color15)";
        event.target.style.color = "var(--color4)";
    }
});

// Як знайти і прибрати пробіли з середини з текстового значення елементу 
// з декількох слів з пробілами між ними (textContent або innerText)?

// Можна використати метод trim() для видалення пробілів з початку і кінця рядка, але він не видаляє пробіли всередині рядка. 
// Щоб видалити пробіли всередині тексту, можна використати регулярний вираз та метод replace().

// `replace(/\s/g, '')` видаляє всі пробіли (\s) в середині тексту. Таким чином, отримаємо текст без пробілів усередині.

// Розберемо цей регулярний вираз покроково:
// 1. `/`: Це початок регулярного виразу. Всі регулярні вирази в JavaScript мають починатися з цього символу.
// 2. `\s`: Це метасимвол, який відповідає будь-якому пробілові символу, такому як пробіл, табуляція, або символ нового рядка.
// 3. `/`: Це закриваючий слеш, який вказує на кінець регулярного виразу.
// 4. `g`: Цей флаг глобального пошуку. Коли цей флаг встановлено, регулярний вираз шукає всі входження, 
// а не зупиняється після знаходження першого входження.

// Отже, `/ \s / g` вказує на всі входження пробілових символів у тексті. 
// У випадку методу `replace()`, вони замінюються на порожній рядок, тобто вони видаляються.




// // 3. Секція What people say about the Ham (карусель, слайдер)
const ul = document.querySelector(".people-slider-center");
console.log(ul);

const sliderList = document.querySelectorAll(".people-slider-center li")
console.log(sliderList);  // знайшли весь список

const arrowPrev = document.querySelector("[data-arrow='prev']");
console.log(arrowPrev); // знайшли ліву стрілочку

const arrowNext = document.querySelector("[data-arrow='next']");
console.log(arrowNext); // знайшли праву стрілочку

let peopleOnePerson = document.querySelector(".people-one-person");
console.log(peopleOnePerson);
console.log(peopleOnePerson.src); // велике фото

let peopleSayPersonName = document.querySelector(".people-say-person-name"); // Name
let peopleSayPersonProf = document.querySelector(".people-say-person-profession");  // Profession
let peopleSayPersonText = document.querySelectorAll(".people-say-text");  // Text


// ПЕРШИЙ варіант руху слайдера по стрілочкам ліворуч, праворуч,
// рух йде виключно по колу, але не працює анімація, тобто немає плавності руху.

// Додаємо подію кліка на стрілочку "prev" (рух праворуч)
arrowPrev.addEventListener("click", () => {

    // Переміщаємо останній елемент на початок (перед першим елементом)
    ul.insertBefore(ul.lastElementChild, ul.firstElementChild);
});

// Додаємо подію кліка на стрілочку "next" (рух ліворуч)
arrowNext.addEventListener("click", () => {

    // Переміщаємо перший елемент в кінець
    ul.appendChild(ul.firstElementChild);   
});

// ДРУГИЙ варіант, схожий на перший, але з масивом, на прикладі однієї кнопки prev
// // Додаємо подію кліка на стрілочку "prev"
// arrowPrev.addEventListener("click", () => {
//     let sliderListArray = Array.from(sliderList);
// 
//     // let leftSlide = sliderListArray.shift();

//     // sliderListArray.push(leftSlide);

//     // // Оновлюємо вміст ul з оновленим масивом слайдів
//     // ul.innerHTML = "";
//     // sliderListArray.forEach(slide => {
//     //     ul.appendChild(slide);
//     // });
//     // // updatePeopleOnePersonSrc()  
// });

// Додаємо прослуховування на клік по фото, шукаємо найближчий li і img
// З li витягуємо дата-атрибути, а з img посилання на фото
ul.addEventListener("click", (event) => {
    const clickedLi = event.target.closest('li');
    if (!clickedLi) return;

    const clickedImg = event.target.closest('img');
    if (!clickedImg) return;
    console.log(clickedImg);
    console.log(clickedImg.src);

    peopleOnePerson.src = clickedImg.src;

    peopleSayPersonName.textContent = clickedLi.dataset.name;

    peopleSayPersonProf.textContent = clickedLi.dataset.prof;

    peopleSayPersonText.forEach((item) => {
        if (item.dataset.name === clickedLi.dataset.name) {
            item.hidden = false;
        } else {
            item.hidden = true;
        }
    })
});



// Додаємо плавний скролінг по сайту з елементів навігації в хедері до відповідних секцій
// - вибирає всі посилання, які мають href, що починається з #,
// - потім забороняємо стандартну дію переходу за посиланням.
document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (elem) {
        elem.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });

        // Другий спосіб:
        // const targetId = this.getAttribute('href').substring(1);
        // отримує ідентифікатор секції з href посилання, вирізаючи символ #.

        // const targetElement = document.getElementById(targetId);
        // знаходить елемент секції за його ідентифікатором, тобто назвою без символа #.

        // if (targetElement) {
        //     window.scrollTo({
        //         top: targetElement.offsetTop,
        //         behavior: 'smooth'
        //     });
        // }       
    });
});



// Додаємо функцію повертання на початок сторінки при кліку на логотип в хедері
document.getElementById('logo').addEventListener('click', function (aLogo) {

    aLogo.preventDefault();

    window.scrollTo({
        top: 0,
        behavior: "smooth"
    })

    // location.reload(); // Перезавантаження сторінки
});



// Фідбек від ментора:
// Оцінка: 91
// Коментарі: Фідбек по степу:

// + 1.https://prnt.sc/jGT-a5qGrxgw
// Немає підкреслення під навігацією при ховері на елемент .
// + Також, бажано, щоб лупа була також посиланням.

// + 2.В цій секції має бути затемнення. https://prnt.sc/uc_4pfPhO-kP

// + 3.https://prnt.sc/sPMPgFeuXXyV
// немає активного класу на табах, який би вказував на те , яка категорія наразі обрана.
// Немає ховеру на зображеннях.

// + 4. Мазонри цікаво, але ще б допрацювати . Бо кожне зображення має бути повноцінним, а у вас вони маленькі. Тобто виглядає не зовсім презентабельно, але реалізовано принаймні.

// - 5.1. Слайдер адванс трохи недороблений.

// + 5.2. Слайдер в звичайній папці:

// Працює коректно, але для ідеалу не вистачає
// а) https://prnt.sc/nSmROT4d1A4J - активне зображення має бути обведене зеленим кружечком.
// б) активне зображення має підійматись вгору . Обране. А у вас воно статичне . Тобто припідняте тільки друге справа. В ідеалі - мають підійматись всі , на яке клікнув , чи перемкнув стрілочкою

// До іншого зауважень не маю .

// З наступающим Новим Роком вас , Ольго!
// Бережіть себе !

















// // // 3. Секція What people say about the Ham (карусель, слайдер)
// // // Варіант, коли статично був закріплен 3-ій елемент (позиція в масиві 2) і при кліку всі рухались в це місце
// // // (при розкоментуванні додати зміну текстових параграфів відповідно до клікнутого фото)
// const ul = document.querySelector(".people-slider-center");
// console.log(ul);

// const sliderList = document.querySelectorAll(".people-slider-center li")
// console.log(sliderList);  // знайшли весь список

// const arrowPrev = document.querySelector("[data-arrow='prev']");
// console.log(arrowPrev); // знайшли ліву стрілочку

// const arrowNext = document.querySelector("[data-arrow='next']");
// console.log(arrowNext); // знайшли праву стрілочку

// let peopleOnePerson = document.querySelector(".people-one-person");
// console.log(peopleOnePerson);
// console.log(peopleOnePerson.src); // велике фото

// let peopleSayPersonName = document.querySelector(".people-say-person-name"); // Name
// let peopleSayPersonProf = document.querySelector(".people-say-person-profession");  // Profession

// // ПЕРШИЙ варіант руху слайдера по стрілочкам ліворуч, праворуч,
// // рух йде виключно по колу, але не працює анімація, тобто немає плавності руху.
// // Тільки на місці третьої li (індекс в масиві 2) збільшується фото і підтягується відповідне ім'я і професія
// // По кліку на будь-яке фото в слайдері, воно займає третю позицію (індекс 2), 
// // всі інші відповідно теж змінюють своє положення:

// // Додаємо подію кліка на стрілочку "prev" (рух праворуч)
// arrowPrev.addEventListener("click", () => {

//     // Переміщаємо останній елемент на початок (перед першим елементом)
//     ul.insertBefore(ul.lastElementChild, ul.firstElementChild);
    
//     updatePeopleOnePersonSrc()  // функція для оновлення відповідного великого фото і ім'я і професії
// });

// // Додаємо подію кліка на стрілочку "next" (рух ліворуч)
// arrowNext.addEventListener("click", () => {
      
//     // Переміщаємо перший елемент в кінець
//     ul.appendChild(ul.firstElementChild); 

//     updatePeopleOnePersonSrc()   
// });

// // ДРУГИЙ варіант, схожий на перший, але з масивом, на прикладі однієї кнопки prev
// // // Додаємо подію кліка на стрілочку "prev"
// // arrowPrev.addEventListener("click", () => {
// //     let sliderListArray = Array.from(sliderList);
// // 
// //     // let leftSlide = sliderListArray.shift();

// //     // sliderListArray.push(leftSlide);

// //     // // Оновлюємо вміст ul з оновленим масивом слайдів
// //     // ul.innerHTML = "";
// //     // sliderListArray.forEach(slide => {
// //     //     ul.appendChild(slide);
// //     // });
// //     updatePeopleOnePersonSrc()  
// // });

// // Функція для оновлення відповідного великого фото, ім'я і професії
// function updatePeopleOnePersonSrc() {
//     const liThirdImg = document.querySelector(".people-slider-center li:nth-of-type(3) img"); //фото третьої li в моменті
//     peopleOnePerson.src = liThirdImg.src;

//     const liThirdName = document.querySelector(".people-slider-center li:nth-of-type(3)").dataset.name; //ім'я третьої li в моменті
//     peopleSayPersonName.textContent = liThirdName;
       
//     const liThirdProf = document.querySelector(".people-slider-center li:nth-of-type(3)").dataset.prof; //професія третьої li в моменті
//     peopleSayPersonProf.textContent = liThirdProf;
// }

// ul.addEventListener("click", (event) => {
//     const clickedLi = event.target.closest('li');
//     if (!clickedLi) return;

//     const sliderList = Array.from(ul.children); // Перетворюємо колекцію li на масив
    
//     const originalOrder = sliderList.indexOf(clickedLi);
//     console.log(originalOrder);

//     // Варіант руху розрахунку кроків до третього слайдера (індекс 2) вперед по колу за годинниковою стрілкою
//     const steps = (2 - originalOrder + sliderList.length) % sliderList.length;
//     console.log(steps);

//     for (let i = 0; i < steps; i++) {
//         ul.insertBefore(ul.lastElementChild, ul.firstElementChild);
//     }

//     // Варіант руху розрахунку кроків до третього слайдера (індекс 2) назад по колу проти годинниковою стрілкою
//     // const steps = (originalOrder - 2 + sliderList.length) % sliderList.length;
//     // console.log(steps);


//     // for (let i = 0; i < steps; i++) {
//     //     ul.appendChild(ul.firstElementChild);
//     // }

//     updatePeopleOnePersonSrc();    
// });
